﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment3
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Title: A3 - Future Value Calculator");
            Console.WriteLine("Author: Casey B. Hargarther");
            DateTime rightNow = DateTime.Now;
            string format = "ddd M/d/yy h:mm:ss t";
            Console.Write("Now: ");
            Console.WriteLine(rightNow.ToString(format));
            Console.WriteLine();

            String balance;
            double finalBalance;

            Console.Write("Starting Balance: ");

            while (!double.TryParse(balance = Console.ReadLine(), out finalBalance))
            {
                Console.WriteLine("Balance must be a number");
                Console.Write("Starting Balance: ");
            }

            String term;
            int finalTerm;

            Console.WriteLine();
            Console.Write("Term (years): ");
            while (!Int32.TryParse(term = Console.ReadLine(), out finalTerm))
            {
                Console.WriteLine("Term must be integer data type");
                Console.Write("Term (years): ");
            }
            Console.WriteLine();


            String interest;
            double finalInterest;
            double temp1;


            Console.Write("Interest Rate: ");
            
            while (!double.TryParse(interest = Console.ReadLine(), out temp1))
            {
                Console.WriteLine("Starting Balance must be numeric");
                Console.Write("Starting Balance: ");
            }

            Console.WriteLine();
            finalInterest = temp1 / 100;

            String deposit;
            double finalDeposit;
         
            Console.Write("Deposit (monthly): ");

            while (!double.TryParse(deposit = Console.ReadLine(), out finalDeposit))
            {
                Console.WriteLine("Monthly deposit must be numeric");
                Console.Write("Deposit (monthly): ");
            }

            Console.WriteLine();

            Console.WriteLine("*** Future Value ***");
            Console.WriteLine("{0:C}", FutureValue(finalBalance, finalTerm, finalInterest, finalDeposit));
            Console.WriteLine();
            Console.WriteLine("Press Enter to exit...");
            Console.Read();

        }

        public static double FutureValue(double theBalance, int theTerm, double theInterest, double theDeposit)
        {

            double futureValue;

            futureValue = (theBalance * Math.Pow(1 + (theInterest / 12), theTerm * 12)) + 
                (theDeposit * ((Math.Pow(1 + (theInterest / 12),  theTerm * 12 ) - 1) / (theInterest / 12)));


            return futureValue;
        }


    }
}

